package pl.edu.uw.dsk.rekrutacja.javadev;

import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

class UserRestControllerTest {

    UserRestController userRestController = new UserRestController();

    @Test
    void change_license_type_should_be_blocked_after_10_times() {
        IntStream.range(1, 11).forEach((i) -> {
            try {
                System.out.println(i + ": Changing license type...");
                System.out.println(i + ": " + userRestController.changeLicenseType(1));
            } catch (Exception e) {
                System.err.println(i + ": " + e.getMessage());
            }
        });
    }

}

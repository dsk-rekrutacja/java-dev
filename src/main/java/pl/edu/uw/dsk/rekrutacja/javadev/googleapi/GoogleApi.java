package pl.edu.uw.dsk.rekrutacja.javadev.googleapi;

import pl.edu.uw.dsk.rekrutacja.javadev.googleapi.exceptions.GoogleApiRateLimitReachedApiException;
import pl.edu.uw.dsk.rekrutacja.javadev.googleapi.exceptions.GoogleApiUnreachableException;

import java.util.Optional;

// Biblioteka dostarczona przez google, nie podlega code-review ani refaktoryzacji :)
public interface GoogleApi {

    /**
     * Get account by email or return empty if not exists.
     *
     * @throws GoogleApiRateLimitReachedApiException if daily rate limit is reached
     * @throws GoogleApiUnreachableException if some network/auth issue happened
     */
    Optional<GoogleApiAccount> getByEmail(String email);

    /**
     * Changes account license type. If user already have specified licenseType then no error is returned
     *
     * @throws GoogleApiRateLimitReachedApiException if daily rate limit is reached
     * @throws GoogleApiUnreachableException if some network/auth issue happened
     */
    Optional<GoogleApiAccount> changeLicenseType(GoogleApiAccount googleAccount, GoogleApiAccount.LicenseType licenseType);

}



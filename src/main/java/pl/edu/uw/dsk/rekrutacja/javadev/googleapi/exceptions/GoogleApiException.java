package pl.edu.uw.dsk.rekrutacja.javadev.googleapi.exceptions;


// Biblioteka dostarczona przez google, nie podlega code-review ani refaktoryzacji :)
public abstract class GoogleApiException extends RuntimeException {

    public GoogleApiException(String message) {
        super(message);
    }
    
}

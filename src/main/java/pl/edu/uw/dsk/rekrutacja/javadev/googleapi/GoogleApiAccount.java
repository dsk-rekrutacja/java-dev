package pl.edu.uw.dsk.rekrutacja.javadev.googleapi;

// Biblioteka dostarczona przez google, nie podlega code-review ani refaktoryzacji :)
public interface GoogleApiAccount {

    String id();

    String email();

    LicenseType licenseType();

    public enum LicenseType {
        BASIC,
        ADVANCED
    }

}

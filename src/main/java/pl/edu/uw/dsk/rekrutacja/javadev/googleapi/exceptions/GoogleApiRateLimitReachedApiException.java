package pl.edu.uw.dsk.rekrutacja.javadev.googleapi.exceptions;


// Biblioteka dostarczona przez google, nie podlega code-review ani refaktoryzacji :)
public class GoogleApiRateLimitReachedApiException extends GoogleApiException {

    public GoogleApiRateLimitReachedApiException() {
        super("google api rate limit reached");
    }

}

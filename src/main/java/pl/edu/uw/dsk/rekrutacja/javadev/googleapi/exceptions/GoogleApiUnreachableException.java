package pl.edu.uw.dsk.rekrutacja.javadev.googleapi.exceptions;


// Biblioteka dostarczona przez google, nie podlega code-review ani refaktoryzacji :)
public class GoogleApiUnreachableException extends GoogleApiException {

    public GoogleApiUnreachableException() {
        super("google api unreachable");
    }
}

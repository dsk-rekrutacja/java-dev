package pl.edu.uw.dsk.rekrutacja.javadev;

import pl.edu.uw.dsk.rekrutacja.javadev.googleapi.GoogleApi;
import pl.edu.uw.dsk.rekrutacja.javadev.googleapi.GoogleApiAccount;
import pl.edu.uw.dsk.rekrutacja.javadev.googleapi.GoogleApiImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

//@RestController
//@RequestMapping("/user/license")
public class UserRestController {

    //@Autowired
    private SecurityContext securityContext = new SecurityContext();

    //@Autowired
    private GoogleApi googleApi = new GoogleApiImpl();

    //@Autowired
    private UserRepository userRepository = new UserRepository();

    private Map<Integer, Integer> usages = new HashMap<>();

    //@GetMapping
    public String changeLicenseType(
            //@RequestParam
            int licenseType
    ) {
        int userId = securityContext.getLoggedUserIdOrElseThrow(new RuntimeException("Nie jesteś zalogowany"));
        UserEntity user = userRepository.findById(userId).orElse(null);
        verifyNumberOfUsages(userId);

        try {


            if (user != null) {

                Optional<GoogleApiAccount> googleAccount = googleApi.getByEmail(user.getGoogleEmail());

                if(googleAccount.isPresent()) {
                    if(user.getStatus() == "STUDENT" || user.getStatus() == "PRACOWNIK") {
                        changeLicenceType(licenseType, googleAccount);
                        return "Zmieniłem typ licencji";
                    } else {
                        return "Nie masz uprawnień do zmiany typu licencji";
                    }
                }

            }
        } catch (Throwable e) {
            System.err.println(e.getMessage());
            return "Coś poszło nie tak";
        }

        return "Nie udało się zmienić licencji";
    }

    private void verifyNumberOfUsages(int userId) {
        if(usages.containsKey(userId)) {
            int numberOfUsages = usages.get(userId);
            usages.put(userId, numberOfUsages + 1);
        } else {
            usages.put(userId, 1);
        }

        if(usages.get(userId) >= 10) {
            throw new RuntimeException("Przekroczyłeś dzienny limit zmian typu licencji. Spróbuj ponownie jutro.");
        }
    }

    private void changeLicenceType(int licenseType, Optional<GoogleApiAccount> googleAccount) {
        if(licenseType == 0) {
            googleApi.changeLicenseType(googleAccount.get(), GoogleApiAccount.LicenseType.BASIC);
        }
        else if(licenseType == 1) {
            googleApi.changeLicenseType(googleAccount.get(), GoogleApiAccount.LicenseType.BASIC);
        }
        else {
            throw new RuntimeException("Niepoprawny typ licencji " + licenseType);
        }
    }

}

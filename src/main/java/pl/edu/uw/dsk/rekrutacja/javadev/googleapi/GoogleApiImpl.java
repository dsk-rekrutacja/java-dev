package pl.edu.uw.dsk.rekrutacja.javadev.googleapi;

import pl.edu.uw.dsk.rekrutacja.javadev.googleapi.exceptions.GoogleApiRateLimitReachedApiException;
import pl.edu.uw.dsk.rekrutacja.javadev.googleapi.exceptions.GoogleApiUnreachableException;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;

// Biblioteka dostarczona przez google, nie podlega code-review ani refaktoryzacji :)
public class GoogleApiImpl implements GoogleApi {

    private final Random random = new Random();

    public Optional<GoogleApiAccount> getByEmail(String email) {

        // imitujemy wolną odpowiedź z api google
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        // imitujemy problemy z siecią
        if(random.nextBoolean()) {
            throw new GoogleApiUnreachableException();
        }

        // imitujemy osiągnięcie limitu zapytań danego dnia
        if(random.nextBoolean()) {
            throw new GoogleApiRateLimitReachedApiException();
        }

        if(Objects.equals(email, "j.kowalski@student.uw.edu.pl")) {
            GoogleApiAccount googleApiAccount = new GoogleApiAccount() {

                @Override
                public String id() {
                    return "eiC0cowoZoog6fah";
                }

                @Override
                public String email() {
                    return "j.kowalski@student.uw.edu.pl";
                }

                @Override
                public LicenseType licenseType() {
                    return LicenseType.ADVANCED;
                }
            };
            return Optional.of(googleApiAccount);
        }
        return Optional.empty();
    }


    public Optional<GoogleApiAccount> changeLicenseType(GoogleApiAccount googleApiAccount, GoogleApiAccount.LicenseType licenseType) {
        return Optional.of(new GoogleApiAccount() {
            @Override
            public String id() {
                return googleApiAccount.id();
            }

            @Override
            public String email() {
                return googleApiAccount.email();
            }

            @Override
            public LicenseType licenseType() {
                return licenseType;
            }
        });
    }

}

package pl.edu.uw.dsk.rekrutacja.javadev;

import java.util.Optional;

//@Repository
public class UserRepository {

    Optional<UserEntity> findById(int userId) {
        if(userId == 123) {
            return Optional.of(new UserEntity(123, "STUDENT", "j.kowalski@student.uw.edu.pl"));
        } else {
            return Optional.empty();
        }
    }

}

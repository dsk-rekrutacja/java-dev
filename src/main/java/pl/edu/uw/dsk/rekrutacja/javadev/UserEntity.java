package pl.edu.uw.dsk.rekrutacja.javadev;

import java.util.Objects;

//@Entity
public class UserEntity {

    //@Id
    private int userId;

    //@Column("status")
    private String status; //STUDENT, PRACOWNIK, ABSOLWENT lub BRAK

    //@Column("googleEmail")
    private String googleEmail; //np. j.kowalski@student.uw.edu.pl

    public UserEntity(int userId, String status, String googleEmail) {
        this.userId = userId;
        this.status = status;
        this.googleEmail = googleEmail;
    }

    public int getUserId() {
        return userId;
    }

    public String getStatus() {
        return status;
    }

    public String getGoogleEmail() {
        return googleEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return userId == that.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }
}

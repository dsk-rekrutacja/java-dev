# Rekrutacja DSK-DEV

> Należy zbudować projekt. Zapoznać się z historyjką użytkownika i wykonać code-review, opowiedzieć co by się zmieniło, wykonać niektóre ze zmian (refaktoryzacja).

> W trakcie zadanie można korzystać ze wszelkich pomocy naukowych (google, stackoverflow), tak jak ma to miejsce w prawdziwej pracy.

## Budowanie projektu
1. Żeby uruchomić projekt, należy mieć zainstalowaną Javę 11.
2. Projekt można uruchomić poprzez gradle lub maven, są również dostarczone wrappery.
3. Żeby nie pobierać zbyt wielu zależności, udajemy, że projekt jest napisany w Springu (adnotacje są zakomentowane).
4. Jedyną dostarczoną zależnością jest JUNIT.

## Historyjka użytkownika

**Usługa REST pozwalająca `osobie` na samodzielną zmianę `typu licencji` do `konta google` (gmail) poprzez portal samoobsługi pracowniczej i studenckiej.**

Wymagania biznesowe:

* `Osoba` jest zalogowana i ma aktywny status studenta lub pracownika.
* `Osoba` może zmienić `typ licencji` maksymalnie 10 razy dziennie, ponieważ API do google posiada rate-limit i nie chcemy, żeby ktoś go nadużywał.
